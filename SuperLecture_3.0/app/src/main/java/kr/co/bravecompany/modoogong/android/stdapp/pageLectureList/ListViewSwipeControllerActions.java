package kr.co.bravecompany.modoogong.android.stdapp.pageLectureList;

public abstract class ListViewSwipeControllerActions {
    public void onLeftClicked(int position) {}
    public void onRightClicked(int position) {}
}
