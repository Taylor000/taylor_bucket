package kr.co.bravecompany.modoogong.android.stdapp.db;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import kr.co.bravecompany.api.android.stdapp.api.OnResultListener;
import kr.co.bravecompany.api.android.stdapp.api.data.LectureItemVO;
import kr.co.bravecompany.api.android.stdapp.api.data.LectureResult;
import kr.co.bravecompany.api.android.stdapp.api.data.Packet;
import kr.co.bravecompany.api.android.stdapp.api.requests.SubscribeLectureRequests;
import okhttp3.Request;


public class MetaDataManager {

    final String TAG = "MetaData";

    private static MetaDataManager instance;

    public static MetaDataManager getInstance() {

        if (instance == null) {
            instance = new MetaDataManager();
        }
        return instance;
    }

    public enum GlobalDataType
    {
        TEACHER_LIST,
        SUBJECT_LIST
    }

    public  interface OnGlobalDataListener
    {
        void onGlobalDataUpdated(GlobalDataType dataType);
    }

    public List<Packet.SubjectData> mSubjectDataList = new ArrayList<>();
    public List<Packet.TeacherData> mTeacherDataList = new ArrayList<>();
    public ArrayList<LectureItemVO> mEndLectureList = new ArrayList<>();
    public ArrayList<LectureItemVO> mIngLectureList = new ArrayList<>();

    //OnGlobalDataListener mGlobalDataListener;

    public void storeLectureDataEndList(LectureResult result) {

        //studies가 null인 상태를 발견하여 studies == null상태일때 메소드를 그냥 리턴.
        if (result == null || result.getStudies() ==null) {
            return;
        }

        ArrayList<LectureItemVO> lectures = result.getStudies();
        mEndLectureList.clear();
        mEndLectureList.addAll(result.getStudies());
    }

    public void storeLectureDataIngList(LectureResult result) {

        if (result == null || result.getStudies() ==null) {
            return;
        }

        ArrayList<LectureItemVO> lectures = result.getStudies();
        mIngLectureList.clear();
        mIngLectureList.addAll(result.getStudies());
    }

    public void removedLectureDataIngList(LectureItemVO item)
    {
        mIngLectureList.remove(item);
    }

    public void storeSubjectDataList()
    {

    }

    public void storeTeacherDataList()
    {

    }

    public LectureItemVO querySubscribedLectureInfo(String lectureCode)
    {
        int iLecCode = Integer.parseInt(lectureCode);

        for (LectureItemVO lectureItemVO : mIngLectureList) {
            if(lectureItemVO.getLectureCode() ==    iLecCode )
            {
                return lectureItemVO;
            }
        }

        return null;
    }


    public void loadTeacherData(final OnGlobalDataListener listener) {

        if (mTeacherDataList.size() == 0) {
            SubscribeLectureRequests.getInstance().requestTeacherList(new OnResultListener<Packet.ResGetTeacherList>() {
                @Override
                public void onSuccess(Request request, Packet.ResGetTeacherList result) {
                    if (result.resultCode >= 0) {
                        Log.d(TAG, String.format("requestTeacherList return count %d", result.tchList.size()));

                        mTeacherDataList.clear();
                        mTeacherDataList.addAll(result.tchList);
                        listener.onGlobalDataUpdated(GlobalDataType.TEACHER_LIST);
                    } else {
                        Log.d(TAG, String.format("requestTeacherList return error %d", result.resultCode));
                    }
                }

                @Override
                public void onFail(Request request, Exception exception) {
                    Log.d(TAG, exception.getMessage());
                }
            });
        } else {
            listener.onGlobalDataUpdated(GlobalDataType.TEACHER_LIST);
        }
    }

    public void loadSubjectData(final OnGlobalDataListener listener) {
        if(mSubjectDataList.size() == 0) {

            SubscribeLectureRequests.getInstance().requestSubjectList(new OnResultListener<Packet.ResGetSubjectList>() {
                @Override
                public void onSuccess(Request request, Packet.ResGetSubjectList result) {
                    if (result.resultCode >= 0) {

                        Log.d(TAG, String.format("requestSubjectList return count %d", result.subjList.size()));

                        mSubjectDataList.clear();
                        mSubjectDataList.addAll(result.subjList);

                        listener.onGlobalDataUpdated(GlobalDataType.SUBJECT_LIST);

                    } else {
                        Log.d(TAG, String.format("requestSubjectList return error : %d", result.resultCode));
                    }
                }

                @Override
                public void onFail(Request request, Exception exception) {

                    Log.d(TAG, exception.getMessage());
                }
            });
        }
        else
        {
            listener.onGlobalDataUpdated(GlobalDataType.SUBJECT_LIST);
        }

    }
}
