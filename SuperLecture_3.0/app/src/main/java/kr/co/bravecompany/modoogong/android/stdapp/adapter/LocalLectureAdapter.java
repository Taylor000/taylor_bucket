package kr.co.bravecompany.modoogong.android.stdapp.adapter;

import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import kr.co.bravecompany.modoogong.android.stdapp.R;
import kr.co.bravecompany.modoogong.android.stdapp.db.model.Lecture;
import kr.co.bravecompany.modoogong.android.stdapp.viewholder.LocalLectureItemViewHolder;
import kr.co.bravecompany.modoogong.android.stdapp.viewholder.OnItemClickListener;
import kr.co.bravecompany.modoogong.android.stdapp.data.LocalLectureData;

/**
 * Created by BraveCompany on 2016. 10. 24..
 */

public class LocalLectureAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<LocalLectureData> items = new ArrayList<LocalLectureData>();
    ProgressBar progressBar; // view_lecture_item.xml에 있는 프로그래스 바 정보를 담을 프로그래스 바 선언.
    LinearLayout linearLayout; // 프로그레스 바가 존재 했던 리니어 레이아웃을 제거 하기 위해 선언.
    public LocalLectureData getItem(int position){
        return items.get(position);
    }

    public int getItemWithStudyLectureNo(int studyLectureNo){
        for(int i=0; i<items.size(); i++){
            Lecture item = items.get(i).getLectureVO();
            if(studyLectureNo == item.getStudyLectureNo()){
                return i;
            }
        }
        return -1;
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    public void add(LocalLectureData LocalLectureData) {
        items.add(LocalLectureData);
        notifyDataSetChanged();
    }

    public void addAll(List<LocalLectureData> items) {
        this.items.addAll(items);
        notifyDataSetChanged();
    }


    private OnItemClickListener mListener;
    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        Log.d("onCreateviewholder", "GO TO DOWLOAD LOCKER !!!"); // 다운로드 보관함 클릭 했다는 로그.
        View view = inflater.inflate(R.layout.view_lecture_item, parent, false);

        progressBar = view.findViewById(R.id.progLectureBar); // 프로그래스 바 id값 저장.
        progressBar.setVisibility(View.GONE); // 다운로드 보관함를 클릭하였으면, view_lecture_item.xml 내에 프로그래스 바를 없애도록 설정.

        linearLayout = view.findViewById(R.id.linearLayoutInLecture); // 리니어 레이아웃 id값 저장.
        linearLayout.setVisibility(View.GONE); // 다운로드 보관함를 클릭하였으면, view_lecture_item.xml 내에 리니어 레이아웃를 없애도록 설정.
        return new LocalLectureItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        LocalLectureItemViewHolder h = (LocalLectureItemViewHolder)holder;
        h.setLocalLectureItem(items.get(position));
        h.setOnItemClickListener(mListener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
