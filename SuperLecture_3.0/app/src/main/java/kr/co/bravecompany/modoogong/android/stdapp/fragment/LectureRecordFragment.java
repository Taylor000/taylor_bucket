package kr.co.bravecompany.modoogong.android.stdapp.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kr.co.bravecompany.modoogong.android.stdapp.R;
import kr.co.bravecompany.modoogong.android.stdapp.activity.LectureSubscribeActivity;

public class LectureRecordFragment extends Fragment {

    private PieChart pieChart;
    private HorizontalBarChart horizontalBarChart;
    private LineChart lineChart;
    private NestedScrollView nestedScrollView;
    private FloatingActionButton floatingActionButton;
    private CircleImageView circleImageView;
    private ImageButton profile_Image_Button;
    private ImageButton profile_add_Friend_Image_Button;
    private ImageButton profile_delete_Friend_Image_Button;

    private Boolean isClicked;

    //private ArrayList<ImageButton> imageButtonArrayList = new ArrayList<ImageButton>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup)inflater.inflate(R.layout.fragment_lecture_record, container, false);

        isClicked = false;

        Init_Layout(rootView);// 레이아웃 초기화
        Init_Listener();
        Init_PieChart(); //이번주 순공시간(파이 그래프)
        Init_HorizontalChart(); //이번주 과목별 순공시간(가로 막대 그래프)
        Init_TransitionChart(); //과목별 학습 추이(라인 그래프)

        return rootView;


    }


    protected void Init_Layout(ViewGroup rootView)
    {

        pieChart = (PieChart)rootView.findViewById(R.id.piechart);
        horizontalBarChart = (HorizontalBarChart)rootView.findViewById(R.id.horizontal_chart);
        lineChart = (LineChart)rootView.findViewById(R.id.linechart);
        nestedScrollView = (NestedScrollView)rootView.findViewById(R.id.scrollView);
        floatingActionButton = (FloatingActionButton)rootView.findViewById(R.id.fab_btn1);
        floatingActionButton.setOnClickListener(mFabClickListener);
        circleImageView = (CircleImageView)rootView.findViewById(R.id.profile_image1);

        profile_Image_Button = (ImageButton)rootView.findViewById(R.id.btn_profile_image_editor);
        profile_add_Friend_Image_Button = (ImageButton)rootView.findViewById(R.id.btn_profile_Add_Friend);
        profile_delete_Friend_Image_Button = (ImageButton)rootView.findViewById(R.id.btn_profile_delete_Friend);


    }

    void Init_Listener()
    {

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                // 스크롤 내림
                if(scrollY > oldScrollY)
                {

                    floatingActionButton.hide();
                    Log.d("testtt","Scroll Down");
                }

                if(scrollY < oldScrollY)
                {
                    floatingActionButton.show();
                    Log.d("testtt","Scroll Up");

                }

                if(scrollY == 0)
                {
                    floatingActionButton.show();
                    Log.d("testtt","Top Scroll");

                }

                if(scrollY == (v.getMeasuredHeight() - v.getChildAt(0).getMeasuredHeight()))
                {
                    floatingActionButton.hide();
                    Log.d("testtt","Bottom Scroll");

                }

            }


        });

        // 프로필 이미지 클릭 시, 프로필 사진 편집, 친구 추가 및 삭제 버튼 보이도록 추가.[2019.10.29 테일러]
        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!isClicked)
                {
                    profile_Image_Button.setVisibility(View.VISIBLE);
                    profile_add_Friend_Image_Button.setVisibility(View.VISIBLE);
                    profile_delete_Friend_Image_Button.setVisibility(View.VISIBLE);
                    isClicked = true;
                }
                else
                {
                    profile_Image_Button.setVisibility(View.INVISIBLE);
                    profile_add_Friend_Image_Button.setVisibility(View.INVISIBLE);
                    profile_delete_Friend_Image_Button.setVisibility(View.INVISIBLE);
                    isClicked = false;
                }


            }

        });


    }

    private View.OnClickListener mFabClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {


            int id = v.getId();

            switch (id) {
                case R.id.fab_btn1:
                    Intent intent = new Intent(getActivity().getApplicationContext(), LectureSubscribeActivity.class); //현재 Fragment에서 데이터를 LectureSubscribeActivity로 보낼 Intent
                    startActivity(intent);
                    break;

            }


        }

    };


    void Init_PieChart()
    {
        pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(0.5f,0.5f,0.5f,0.5f);

        pieChart.setDragDecelerationFrictionCoef(20.0f);

        pieChart.setDrawHoleEnabled(false);
        pieChart.setHoleColor(Color.WHITE);
        pieChart.setTransparentCircleRadius(0.1f);


        ArrayList<PieEntry> yValue = new ArrayList<PieEntry>();

        yValue.add(new PieEntry(1f,"국어"));
        yValue.add(new PieEntry(5f,"수학"));


        Description description = new Description();
//        description.setText("순공그래프");
        description.setTextSize(2);
//        pieChart.setDescription(description);

        // pieChart.animateY(1000, Easing.EaseInOutCubic); // 애니메이션

        PieDataSet dataSet = new PieDataSet(yValue,"");
        dataSet.setSliceSpace(1.0f);
        dataSet.setSelectionShift(1.0f);
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        PieData data = new PieData((dataSet));
//        data.setValueTextSize(2f);
//        data.setValueTextColor(Color.YELLOW);


        pieChart.setData(data);

    }


    void Init_HorizontalChart()
    {

        List<BarEntry> entries = new ArrayList<BarEntry>(); //barchart 객체를 사용.

        // 데이터 entries에 추가.
        entries.add(new BarEntry(100,0));
        entries.add(new BarEntry(80,1));
        entries.add(new BarEntry(60,2));
        entries.add(new BarEntry(40,3));

        BarDataSet barDataSet = new BarDataSet(entries,"순공시간");

        BarData barData = new BarData(barDataSet);

        horizontalBarChart.setData(barData);
        horizontalBarChart.setFitBars(false);
        //horizontalBarChart.animateXY(1000,1000); // 애니메이션
        horizontalBarChart.invalidate();

    }


    void Init_TransitionChart()
    {


        ArrayList<Entry> entries = new ArrayList<>();
        entries.add(new Entry(4f, 0));
        entries.add(new Entry(8f, 1));
        entries.add(new Entry(6f, 2));
        entries.add(new Entry(2f, 3));
        entries.add(new Entry(18f, 4));
        entries.add(new Entry(9f, 5));
        entries.add(new Entry(16f, 6));
        entries.add(new Entry(5f, 7));
        entries.add(new Entry(3f, 8));
        entries.add(new Entry(7f, 10));
        entries.add(new Entry(9f, 11));

        LineDataSet dataset = new LineDataSet(entries, "");

        ArrayList<String> labels = new ArrayList<String>();
        labels.add("1일");
        labels.add("2일");
        labels.add("3일");
        labels.add("4일");
        labels.add("5일");
        labels.add("6일");

        LineData data = new LineData(dataset);
        dataset.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataset.setColors(ColorTemplate.COLORFUL_COLORS);
        dataset.setDrawFilled(true);
        /*dataset.setDrawCubic(true); //선 둥글게 만들기
        dataset.setDrawFilled(true); //그래프 밑부분 색칠*/

        lineChart.setData(data);
        //lineChart.animateY(5000);



    }


}


