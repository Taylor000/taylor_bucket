package kr.co.bravecompany.modoogong.android.stdapp.activity;

//! Home Activity에서 발생한 이벤트를 받기 위한 장치
public interface OnHomeActivityEventListener
{
    boolean onHomeBackPressed();
    void onHomeTitleBarPressed();
    void onHomeMenuLecRegClicked();
}
