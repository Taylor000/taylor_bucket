package kr.co.bravecompany.modoogong.android.stdapp.config;

/**
 * Created by BraveCompany on 2017. 2. 10..
 */

public class AnalysisTags {
    //Attribute
    public static final String ACTION = "action";
    public static final String ERROR = "error";
    public static final String VALUE = "value";
    public static final String VIEW = "view";
    public static final String FILE = "file";

    //contentName
    public static final String FAB = "fab";
    public static final String SPLASH = "splash";
    public static final String LOGIN = "login";
    public static final String MAIN = "main";
    public static final String LECTURE = "lecture";
    public static final String STUDY = "study";
    public static final String DOWNLOAD = "download";
    public static final String LOCALLECTURE = "local_lecture";
    public static final String LOCALSTUDY = "local_study";
    public static final String FREESTUDY = "free_study";
    public static final String EXPLAINSTUDY = "explain_study";
    public static final String DOQA = "do_qa";
    public static final String DOQASTUDY = "do_qa_study";
    public static final String DOQAONETOONE = "do_qa_one_to_one";
    public static final String QALIST = "qa_list";
    public static final String QADETAIL = "qa_detail";
    public static final String VOICERECORD = "voice_record";
    public static final String NOTICEDETAIL = "notice_detail";
    public static final String NOLOGIN = "no_login";
    public static final String NONETWORK = "no_network";
    public static final String SETTING = "setting";
    public static final String REQUEST = "request";
}
