package kr.co.bravecompany.modoogong.android.stdapp.config;

/**
 * Created by BraveCompany on 2016. 10. 28..
 */

public class LocalAddress {

    public static final String FOLDER = "/Brave";
    public static final String FOLDER_CAMERA = "/BraveCamera";
    public static final String FOLDER_RECORDER = "/BraveRecorder";
    public static final String NAME = "/Brave";

}
