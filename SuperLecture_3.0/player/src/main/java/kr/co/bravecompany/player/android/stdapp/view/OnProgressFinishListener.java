package kr.co.bravecompany.player.android.stdapp.view;

import android.view.View;

/**
 * Created by BraveCompany on 2016. 11. 4..
 */

public interface OnProgressFinishListener {
    public void onProgressFinish(View progressBar);
}
