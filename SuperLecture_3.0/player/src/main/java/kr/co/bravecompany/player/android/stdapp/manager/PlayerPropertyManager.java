package kr.co.bravecompany.player.android.stdapp.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.view.Surface;


/**
 * Created by BraveCompany on 2016. 11. 9..
 */

public class PlayerPropertyManager {
    private static PlayerPropertyManager instance;
    public static PlayerPropertyManager getInstance(Context context) {
        if (instance == null) {
            instance = new PlayerPropertyManager(context);
        }
        return instance;
    }
    SharedPreferences mPrefs;
    SharedPreferences.Editor mEditor;

    private PlayerPropertyManager(Context context) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        mEditor = mPrefs.edit();
    }

    private static final String AUTO_CONTINUE_PLAY = "auto_continue_play";
    //Kollus
    private static final String HW_CODEC_KEY = "is_hwcodec";
    private static final String SW_PREFER_USER_KEY = "sw_prefer_user";
    //Player
    private static final String SCREEN_ORIENTATION = "orientation";
    private static final String SCREEN_BRIGHTNESS = "brightness";
    private static final String PLAY_RATE = "play_rate";
    private static final String SAVE_BRIGHTNESS = "save_brightness";
    private static final String SAVE_RATE = "save_rate";


    public boolean isAutoContinuePlay() {
        return mPrefs.getBoolean(AUTO_CONTINUE_PLAY, true);
    }

    public void setAutoContinuePlay(boolean autoContinuePlay) {
        mEditor.putBoolean(AUTO_CONTINUE_PLAY, autoContinuePlay);
        mEditor.commit();
    }

    public boolean isSWCodec() {
        return !mPrefs.getBoolean(HW_CODEC_KEY, true);
    }

    public void setSWCodec(boolean isSWCodec) {
        mEditor.putBoolean(HW_CODEC_KEY, !isSWCodec);
        mEditor.putBoolean(SW_PREFER_USER_KEY, isSWCodec);
        mEditor.commit();
    }

    public int getScreenOrientation() {
        return mPrefs.getInt(SCREEN_ORIENTATION, Configuration.ORIENTATION_LANDSCAPE);
    }

    public void setScreenOrientation(int orientation) {
        mEditor.putInt(SCREEN_ORIENTATION, orientation);
        mEditor.commit();
    }

    public int getScreenBrightness() {
        return mPrefs.getInt(SCREEN_BRIGHTNESS, 255);
    }

    public void setScreenBrightness(int brightness) {
        mEditor.putInt(SCREEN_BRIGHTNESS, brightness);
        mEditor.commit();
    }

    public float getPlayRate() {
        return mPrefs.getFloat(PLAY_RATE, 1.0f);
    }

    public void setPlayRate(float rate) {
        mEditor.putFloat(PLAY_RATE, rate);
        mEditor.commit();
    }

    public boolean isSaveBrightness() {
        return mPrefs.getBoolean(SAVE_BRIGHTNESS, true);
    }

    public void setSaveBrightness(boolean save) {
        mEditor.putBoolean(SAVE_BRIGHTNESS, save);
        if(!save) {
            mEditor.remove(SCREEN_BRIGHTNESS);
        }
        mEditor.commit();
    }

    public boolean isSaveRate() {
        return mPrefs.getBoolean(SAVE_RATE, true);
    }

    public void setSaveRate(boolean save) {
        mEditor.putBoolean(SAVE_RATE, save);
        if(!save) {
            mEditor.remove(PLAY_RATE);
        }
        mEditor.commit();
    }

    // =============================================================================
    // Remove
    // =============================================================================

    public void removePlayerPrefs(){
        mEditor.remove(AUTO_CONTINUE_PLAY);
        setSWCodec(false);
        mEditor.remove(SCREEN_ORIENTATION);
        mEditor.remove(SCREEN_BRIGHTNESS);
        mEditor.remove(PLAY_RATE);
        mEditor.remove(SAVE_BRIGHTNESS);
        mEditor.remove(SAVE_RATE);
        mEditor.commit();
    }
}
