package kr.co.bravecompany.player.android.stdapp.data;

import com.kollus.sdk.media.content.KollusBookmark;

/**
 * Created by BraveCompany on 2017. 5. 26..
 */

public class BookmarkData {
    private KollusBookmark bookmark;

    public KollusBookmark getBookmark() {
        return bookmark;
    }

    public void setBookmark(KollusBookmark bookmark) {
        this.bookmark = bookmark;
    }
}
