package kr.co.bravecompany.player.android.stdapp.video;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.SurfaceView;
import android.widget.RelativeLayout;

import com.kollus.sdk.media.KollusPlayerContentMode;

/**
 * Created by BraveCompany on 2016. 11. 29..
 */

public class VideoView extends RelativeLayout {

    private Context mContext;

    private Rect mDisplayRect;
    private Rect mVideoRect;
    private Rect mSurfaceViewRect;
    private int mScreenSizeMode;

    private int mVideoWidth;
    private int mVideoHeight;

    private SurfaceView mSurfaceView;
    private float mScalefactor = 1.0f;


    public VideoView(Context context) {
        this(context, null);
    }

    public VideoView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;

        mDisplayRect = new Rect();
        mVideoRect = new Rect();
        mSurfaceViewRect = new Rect();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        mDisplayRect.set(left, top, right, bottom);
        updateVideoSize(mScreenSizeMode);
    }

    public void initVideoView(SurfaceView surfaceView) {
        mVideoWidth = 0;
        mVideoHeight = 0;
        mSurfaceView = surfaceView;
    }

    public void updateVideoSize(int screenSizeMode) {
        if (mVideoWidth > 0 && mVideoHeight > 0) {
            int l = mDisplayRect.left;
            int t = mDisplayRect.top;
            int r = mDisplayRect.right;
            int b = mDisplayRect.bottom;
            int displayWidth = mDisplayRect.width();
            int displayHeight = mDisplayRect.height();

            mScreenSizeMode = screenSizeMode;
            if(mScreenSizeMode == KollusPlayerContentMode.ScaleAspectFill) {
                if ( mVideoWidth * displayHeight  > displayWidth * mVideoHeight ) {
                    displayWidth = displayHeight * mVideoWidth / mVideoHeight;
                } else if ( mVideoWidth * displayHeight  < displayWidth * mVideoHeight ) {
                    displayHeight = displayWidth * mVideoHeight / mVideoWidth;
                }
            }
            else if(mScreenSizeMode == KollusPlayerContentMode.ScaleAspectFit) {
                if ( mVideoWidth * displayHeight  > displayWidth * mVideoHeight ) {
                    displayHeight = displayWidth * mVideoHeight / mVideoWidth;
                } else if ( mVideoWidth * displayHeight  < displayWidth * mVideoHeight ) {
                    displayWidth = displayHeight * mVideoWidth / mVideoHeight;
                }
            }
            else if(mScreenSizeMode == KollusPlayerContentMode.ScaleCenter) {
                displayWidth = mVideoWidth;
                displayHeight = mVideoHeight;
            }

            if(mScreenSizeMode != KollusPlayerContentMode.ScaleZoom) {
                mScalefactor = 1.0f;

                l = (r-l-displayWidth)/2;
                r = l+displayWidth;
                t = (b-t-displayHeight)/2;
                b = t+displayHeight;
                mVideoRect.set(l, t, r, b);
                mSurfaceViewRect.set(mVideoRect);

                mSurfaceView.layout(l, t, r, b);
            }
            else {
                float scalefactor = mScalefactor+1.0f;

                int width = mVideoRect.width();
                int height = mVideoRect.height();
                int scaleWidth = (int)(width*scalefactor);
                int scaleHeight = (int)(height*scalefactor);

                l = (displayWidth-width)/2;
                r = l+width;
                t = (displayHeight-height)/2;
                b = t+height;
                mSurfaceViewRect.set(l, t, r, b);

                l = (displayWidth-scaleWidth)/2;
                r = l+scaleWidth;
                t = (displayHeight-scaleHeight)/2;
                b = t+scaleHeight;

                mSurfaceView.layout(l, t, r, b);
            }
        }
    }

    public void videoSizeChanged(int videoWidth, int videoHeight){
        mVideoWidth = videoWidth;
        mVideoHeight = videoHeight;

        if (mVideoWidth != 0 && mVideoHeight != 0) {
            mSurfaceView.getHolder().setFixedSize(mVideoWidth, mVideoHeight);
        }
    }

    public int getVideoWidth() {
        return mVideoWidth;
    }

    public int getVideoHeight() {
        return mVideoHeight;
    }

    public boolean canMoveVideoScreen() {
        if (mSurfaceViewRect.width() > mDisplayRect.width() ||
                mSurfaceViewRect.height() > mDisplayRect.height()) {
            return true;
        }
        return false;
    }

    public void moveVideoFrame(float velocityX, float velocityY) {
        if(mSurfaceViewRect.width() > mDisplayRect.width() || mSurfaceViewRect.height() > mDisplayRect.height()) {
            int l = mSurfaceViewRect.left;
            int t = mSurfaceViewRect.top;
            int r = mSurfaceViewRect.right;
            int b = mSurfaceViewRect.bottom;
            int width = mSurfaceViewRect.width();
            int height = mSurfaceViewRect.height();
            int displayWidth = mDisplayRect.width();
            int displayHeight = mDisplayRect.height();

            int moveX = 0;
            int moveY = 0;

            if(width > displayWidth) {
                if(velocityX >= 0) {
                    if((l+(int)velocityX) > 0)
                        moveX = -l;
                    else
                        moveX = (int)velocityX;
                }
                else {
                    if((r+(int)velocityX) < mDisplayRect.right)
                        moveX = mDisplayRect.right - r;
                    else
                        moveX = (int)velocityX;
                }
            }
            else {
                moveX = 0;
                l = (displayWidth-width)/2;
                r = l+width;
            }

            if(height > displayHeight) {
                if(velocityY > 0) {
                    if((t+(int)velocityY) >= 0)
                        moveY = -t;
                    else
                        moveY = (int)velocityY;
                }
                else {
                    if((b+(int)velocityY) < mDisplayRect.bottom)
                        moveY = mDisplayRect.bottom - b;
                    else
                        moveY = (int)velocityY;
                }
            }
            else {
                moveY = 0;
                t = (displayHeight-height)/2;
                b = t+height;
            }

            mVideoRect.set(mVideoRect.left+moveX, mVideoRect.top+moveY, mVideoRect.right+moveX, mVideoRect.bottom+moveY);
            mSurfaceViewRect.set(l+moveX, t+moveY, r+moveX, b+moveY);
            mSurfaceView.layout(l+moveX, t+moveY, r+moveX, b+moveY);
        }
    }

}
