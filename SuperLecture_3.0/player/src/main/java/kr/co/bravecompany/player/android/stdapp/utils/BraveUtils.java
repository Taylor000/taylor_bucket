package kr.co.bravecompany.player.android.stdapp.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.kollus.sdk.media.util.Utils;

import java.io.ByteArrayOutputStream;

import kr.co.bravecompany.player.android.stdapp.R;

/**
 * Created by BraveCompany on 2017. 8. 28..
 */

public class BraveUtils {

    // =============================================================================
    // Image
    // =============================================================================

    public static void setImage(ImageView view, Bitmap bitmap){
        if(bitmap == null){
            Glide.with(view.getContext()).load(Uri.EMPTY).into(view);
        }else{
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();

            if(byteArray == null){
                Glide.with(view.getContext()).load(Uri.EMPTY).into(view);
            }else {
                Glide.with(view.getContext()).load(byteArray).placeholder(R.color.colorPrimaryDark)
                        .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).thumbnail(0.1f).into(view);
            }
        }
    }

    public static void setImage(ImageView view, Bitmap bitmap, int width, int height){
        if(bitmap == null){
            Glide.with(view.getContext()).load(Uri.EMPTY).into(view);
        }else{
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();

            if(byteArray == null){
                Glide.with(view.getContext()).load(Uri.EMPTY).override(width, height).into(view);
            }else{
                Glide.with(view.getContext()).load(byteArray).placeholder(R.color.colorPrimaryDark)
                        .override(width, height).thumbnail(0.1f).into(view);
            }
        }
    }

    // =============================================================================
    // Kollus Player
    // =============================================================================

    public static String formattingTime(int time) {
        int h = time / 3600;
        int m = (time - h * 3600) / 60;
        int s = time - (h * 3600 + m * 60);
        String strTime;
        if (h == 0) {
            strTime = String.format("%02d:%02d", m, s);
        } else {
            strTime = String.format("%d:%02d:%02d", h, m, s);
        }
        return strTime;
    }

    public static String getTotalMemorySize(Context context) {
        return Utils.getStringSize(Utils.getTotalMemorySize(Utils.getStoragePath(context)));
    }

    public static String getAvailableMemorySize(Context context) {
        return Utils.getStringSize(Utils.getAvailableMemorySize(Utils.getStoragePath(context)));
    }

    // =============================================================================
    // Views
    // =============================================================================

    public static void setVisibilityBottomViewLong(final View view, int visibility){
        if(view.getVisibility() != visibility) {
            Animation visible = AnimationUtils.loadAnimation(view.getContext(), R.anim.slide_in_bottom_long);
            Animation gone = AnimationUtils.loadAnimation(view.getContext(), R.anim.slide_out_bottom_long);
            if (visibility == View.VISIBLE) {
                view.setAnimation(visible);
            } else {
                view.setAnimation(gone);
            }
            view.setVisibility(visibility);
        }
    }

    // =============================================================================
    // Toast
    // =============================================================================

    public static void showToast(Activity activity, String msg){
        Toast toast = Toast.makeText(activity, msg, Toast.LENGTH_SHORT);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if( v != null) v.setGravity(Gravity.CENTER);
        toast.show();
    }

    // =============================================================================
    // Alert Dialog
    // =============================================================================

    public static void showAlertDialog(Activity activity, String title, String msg,
                                       String positive, String negative,
                                       MaterialDialog.SingleButtonCallback listenerOk,
                                       MaterialDialog.SingleButtonCallback listenerCancel){
        if(activity.isFinishing()) {
            return;
        }
        new MaterialDialog.Builder(activity)
                .title(title)
                .content(msg)
                .positiveText(positive)
                .onPositive(listenerOk)
                .negativeText(negative)
                .onNegative(listenerCancel)
                .cancelable(false)
                .show();
    }
}
