package kr.co.bravecompany.api.android.stdapp.api.data;

/**
 * Created by BraveCompany on 2017. 1. 16..
 */

public class BoardDelFileRequest {
    private int listNo;

    public int getListNo() {
        return listNo;
    }

    public void setListNo(int listNo) {
        this.listNo = listNo;
    }
}
