package kr.co.bravecompany.api.android.stdapp.api.data;

/**
 * Created by BraveCompany on 2017. 10. 31..
 */

public class StudyFileItemVO {
    private int no;
    private String title;
    private String noti_yn;

    public int getNo() {
        return no;
    }

    public String getTitle() {
        return title;
    }

    public String getNoti_yn() {
        return noti_yn;
    }
}
