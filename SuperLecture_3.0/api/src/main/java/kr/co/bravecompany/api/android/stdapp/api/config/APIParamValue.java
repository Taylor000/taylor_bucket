package kr.co.bravecompany.api.android.stdapp.api.config;

/**
 * Created by BraveCompany on 2016. 11. 7..
 */

public class APIParamValue {
    public static final String CHARSET_UTF8 = "&charset=UTF-8";
    public static final String USERKEY = "&userKey=";
    public static final String MTHD_MS = "&Mthd=MS";
    public static final String PAGE = "&page=";
    public static final String PERPAGE = "&perPage=";
    public static final String STATE = "&State=";
    public static final String LECTURECODE = "&lectureCode=";
    public static final String STUDYMTHD = "&studyMthd=";
    public static final String CATE = "&cate=";
    public static final String SCATE = "&scate=";
    public static final String CODECD = "&codeCd=";
    public static final String BOARDKND = "&boardKnd=";
    public static final String BOARDCATE = "&boardCate=";
    public static final String CATETYP = "&cateTyp=";
    public static final String CHRTYP = "&chrTyp=";
    public static final String BKTYP = "&bkTyp=";
    public static final String TCHTYP = "&tchTyp=";
    public static final String DISPLAYAREA = "&displayArea=";
    public static final String EXAMCLASS = "&examClass=";
    public static final String PG = "&pg=";
    public static final String NO = "&no=";
    public static final String USER_KEY = "&usr_key=";
}
