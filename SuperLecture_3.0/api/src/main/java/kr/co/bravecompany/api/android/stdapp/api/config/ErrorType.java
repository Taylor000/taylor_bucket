package kr.co.bravecompany.api.android.stdapp.api.config;

/**
 * Created by BraveCompany on 2016. 11. 7..
 */

public class ErrorType {

    //login
    public static class LOGIN_USER_STATE {
        public static final String ERROR_ID = "EI";
        public static final String ERROR_PW = "EP";
        public static final String ERROR_MOBILE = "EM";
        public static final String S0 = "S0";
        public static final String S1 = "S1";
        public static final String S2 = "S2";
        public static final String S3 = "S3";
    }
}
