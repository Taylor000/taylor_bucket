package kr.co.bravecompany.api.android.stdapp.api.data;

/**
 * Created by BraveCompany on 2016. 11. 15..
 */

public class BoardCateVO {
    private String CodeKnd;
    private String CodeNm;
    private String CD;
    private String NM;
    private String PCD;
    private String PNM;

    public String getCodeKnd() {
        return CodeKnd;
    }

    public String getCodeNm() {
        return CodeNm;
    }

    public String getCD() {
        return CD;
    }

    public String getNM() {
        return NM;
    }

    public String getPCD() {
        return PCD;
    }

    public String getPNM() {
        return PNM;
    }
}
