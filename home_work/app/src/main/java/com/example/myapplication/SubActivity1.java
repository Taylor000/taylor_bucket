package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class SubActivity1 extends AppCompatActivity {


    private Button button;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub1);

        final Intent intent = getIntent(); // 데이터 수신



        button = findViewById(R.id.subActivitybtn2);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent1 = new Intent(getApplicationContext(),SubActivity2.class);// 다음 액티비티로 전환할 인텐트.

                startActivity(intent1); // 다음 액티비티로 전환.



            }
        });


        toolbar = findViewById(R.id.subtoolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); // 뒤로가기 버튼


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId())
        {


            case android.R.id.home: //이 홈은 툴바의 뒤로가기 기본 세팅 네임(home)
            {
                finish(); // 뒤로가기 호출
                return true;

            }



        }

        return super.onOptionsItemSelected(item);
    }
}
