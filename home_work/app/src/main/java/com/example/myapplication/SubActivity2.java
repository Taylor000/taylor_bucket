package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

public class SubActivity2 extends AppCompatActivity {



    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub2);

        Intent intent = getIntent();


        toolbar = findViewById(R.id.subtoolbar2);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId())
        {


            case android.R.id.home: //이 홈은 툴바의 뒤로가기 기본 세팅 네임(home)
            {
                finish(); // 뒤로가기 호출
                return true;

            }



        }

        return super.onOptionsItemSelected(item);
    }


}
