package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class SubActivity3 extends AppCompatActivity {


    private Button button;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub3);


        final Intent intent = getIntent();


        button = findViewById(R.id.subActivitybtn4);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent1 = new Intent(getApplicationContext(),SubActivity4.class);

                startActivity(intent1);


            }
        });



        toolbar = findViewById(R.id.subtoolbar3);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId())
        {


            case android.R.id.home: //이 홈은 툴바의 뒤로가기 기본 세팅 네임(home)
            {
                finish(); // 뒤로가기 호출
                return true;

            }



        }



        return super.onOptionsItemSelected(item);
    }
}
